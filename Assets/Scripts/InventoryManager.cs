﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Assets.Scripts;
using UnityEngine;

public class InventoryManager
{
    public delegate bool TransactionAction(ItemData data, int count);

    public TransactionAction OnTransaction;
    public Action<InventoryUnit> OnRemoveItem;
    public Action<InventoryUnit> OnAddItem;

    private Action<float> OnMoneyAmountChange;

    private Dictionary<EItemType, List<InventoryUnit>> m_Items = new Dictionary<EItemType, List<InventoryUnit>>();
    private Dictionary<EItemType, int> m_MaxStackCount = new Dictionary<EItemType, int>();
    private bool m_IsVendor;


    public float TotalMoney
    {
        get { return m_TotalMoney; }
        set
        {
            m_TotalMoney = value;
            OnMoneyAmountChange.SafeInvoke(m_TotalMoney);
        }
    }

    private float m_TotalMoney;

    public void CreateInventory(InventoryData data, Action<float> onMoneyChange)
    {
        m_MaxStackCount.Add(EItemType.Weapon, 1);
        m_MaxStackCount.Add(EItemType.QuestItem, 1);
        m_MaxStackCount.Add(EItemType.Material, 5);
        m_MaxStackCount.Add(EItemType.Consumable, 10);
        m_IsVendor = data.IsVendor;
        m_TotalMoney = data.Money;

        OnMoneyAmountChange = onMoneyChange;
        foreach (var itemData in data.CurrentItems)
        {
            AddItem(itemData.ItemData, itemData.Count);
        }
    }

    public List<InventoryUnit> GetItemsByType(EItemType type)
    {
        if (m_Items.ContainsKey(type))
        {
            return m_Items[type];
        }
        return null;
    }

    public List<InventoryUnit> GetAllItems()
    {
        List<InventoryUnit> all = new List<InventoryUnit>();
        foreach (var list in m_Items.Values)
        {
            all.AddRange(list);
        }
        return all;
    }


    public void AddItem(ItemData data, int count)
    {
        EItemType itemType = data.GetType();

        if (!m_Items.ContainsKey(itemType))
        {
            m_Items.Add(itemType, new List<InventoryUnit>());
        }

        int maxCountInOneSlot = m_MaxStackCount[itemType];
        int restAmount = count;
        List<InventoryUnit> units = m_Items[itemType].FindAll(u => u.Data.name == data.name);
        if (units != null && units.Count > 0)
        {
            foreach (var unit in units)
            {
                int amountAfterAdding = unit.Count + restAmount;
                if (amountAfterAdding > maxCountInOneSlot)
                {
                    unit.Count = maxCountInOneSlot;
                }
                else
                {
                    unit.Count += restAmount;
                }

                restAmount = amountAfterAdding - maxCountInOneSlot;
                if (restAmount <= 0)
                    break;
            }
        }

        while (restAmount > 0)
        {
            int amountToAdd = Math.Min(restAmount, maxCountInOneSlot);
            InventoryUnit newUnit = new InventoryUnit();
            newUnit.Init(data, amountToAdd, GetPossibleActions(itemType));
            m_Items[itemType].Add(newUnit);
            OnAddItem.SafeInvoke(newUnit);
            restAmount -= amountToAdd;
        }
    }


    public void HandleAction(InventoryUnit unit, EItemActionType actionType, int count)
    {
        switch (actionType)
        {
            case EItemActionType.Buy:
            case EItemActionType.Sell:
                if (OnTransaction.Invoke(unit.Data, count))
                {
                    DecreaseAmount(unit, count);
                }
                break;

            case EItemActionType.Use:
                DecreaseAmount(unit, count);
                break;
            case EItemActionType.Disassemble:
                WeaponData weaponData = (WeaponData)unit.Data;
                if (weaponData != null)
                {
                    foreach (var material in weaponData.Compilation)
                    {
                        AddItem(material, 1);
                    }
                    RemoveItem(unit);
                }
                break;
        }
    }

    public int GetTotalAmountOfItem(ItemData data)
    {
        EItemType itemType = data.GetType();
        List<InventoryUnit> units = m_Items[itemType].FindAll(u => u.Data.name == data.name);
        if (units != null && units.Count > 0)
        {
            int count = 0;
            foreach (var unit in units)
            {
                count += unit.Count;
            }
            return count;
        }
        return 0;
    }

    //private void IncreaseCount(InventoryUnit unit, int count)
    //{
    //    int amountAfterChange = unit.Count + count;
    //    if (amountAfterChange <= 0)
    //    {
    //        RemoveItem(unit);
    //    }
    //    else if (amountAfterChange > m_MaxStackCount[unit.Data.GetType()])
    //    {
    //        AddItem(unit.Data, count);
    //    }
    //    else
    //    {
    //        unit.Count += count;
    //    }
    //}

    private void DecreaseAmount(InventoryUnit targetUnit, int count)
    {
        ItemData data = targetUnit.Data;
        EItemType itemType = data.GetType();


        List<InventoryUnit> units = m_Items[itemType].FindAll(u => u.Data.name == data.name);
        int totalAmount = GetTotalAmountOfItem(data) - count;
        int maxAmountPerSlot = m_MaxStackCount[data.GetType()];

        int restAmount = totalAmount;
        foreach (var unit in units)
        {
            if (restAmount <= 0)
            {
                RemoveItem(unit);
            }

            unit.Count = Math.Min(maxAmountPerSlot, restAmount);
            restAmount -= unit.Count;
        }
    }

    private void RemoveItem(InventoryUnit unit)
    {
        EItemType itemType = unit.Data.GetType();
        if (m_Items.ContainsKey(itemType))
        {
            OnRemoveItem.SafeInvoke(unit);
            unit.Clear();
            m_Items[itemType].Remove(unit);
        }
    }

    private List<EItemActionType> GetPossibleActions(EItemType itemType)
    {
        List<EItemActionType> actions = new List<EItemActionType>();
        if (m_IsVendor)
        {
            actions.Add(EItemActionType.Buy);
        }
        else
        {
            switch (itemType)
            {
                case EItemType.Consumable:
                    actions.Add(EItemActionType.Sell);
                    actions.Add(EItemActionType.Use);
                    break;
                case EItemType.Material:
                    actions.Add(EItemActionType.Sell);
                    break;
                case EItemType.Weapon:
                    actions.Add(EItemActionType.Disassemble);
                    actions.Add(EItemActionType.Sell);
                    break;
            }
        }
        return actions;
    }
}
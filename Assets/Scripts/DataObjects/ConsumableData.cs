﻿using Assets.Scripts;
using UnityEngine;

[CreateAssetMenu(fileName = "ConsumableData", menuName = "ScriptableObjects/Consumable")]
public class ConsumableData : ItemData
{
    public override EItemType GetType()
    {
        return EItemType.Consumable;
    }   

    
}
﻿using Assets.Scripts;
using UnityEngine;

[CreateAssetMenu(fileName = "QuestItem", menuName = "ScriptableObjects/QuestItem")]
public class QuestItemData : ItemData
{
    public override EItemType GetType()
    {
        return EItemType.QuestItem;
    }
}
﻿using Assets.Scripts;
using UnityEngine;

public class ItemData : ScriptableObject
{
    public string Name = "ItemName";
    public string Description = "Very useful item, take 2!";
    public Sprite Asset;
    public float Price = 10;

    public virtual EItemType GetType()
    {
        return EItemType.None;
    }

    public void InitItem(string name, string description, Sprite asset, float sellPrice)
    {
        Name = name;
        Description = description;
        Asset = asset;
        Price = sellPrice;
    }

}

﻿using Assets.Scripts;
using UnityEngine;

[CreateAssetMenu(fileName = "MaterialData", menuName = "ScriptableObjects/Material")]
public class MaterialData : ItemData
{
    public override EItemType GetType()
    {
        return EItemType.Material;
    }
}
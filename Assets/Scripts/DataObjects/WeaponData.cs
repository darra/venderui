﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/Weapon")]
public class WeaponData : ItemData
{
    public List<MaterialData> Compilation;

    public override EItemType GetType()
    {
        return EItemType.Weapon;
    }
}

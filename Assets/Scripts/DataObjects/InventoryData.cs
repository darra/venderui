﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/Inventory", order = 5)]
public class InventoryData : ScriptableObject
{
    public float Money;
    public bool IsVendor;
    public List<InventoryItemData> CurrentItems;
}

[System.Serializable]
public class InventoryItemData
{
    public ItemData ItemData;
    public int Count;
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlotObjectPool : MonoBehaviour
{
    [SerializeField]
    private UIInventorySlot m_SlotPrefab;

    [SerializeField]
    private int m_InitialCount;


    private Stack<UIInventorySlot> m_FreeSlots = new Stack<UIInventorySlot>();

    private void Awake()
    {
        for(int i = 0; i < m_InitialCount; i++)
        {
            UIInventorySlot slot = Instantiate(m_SlotPrefab, transform, false);
            slot.gameObject.SetActive(false);
            m_FreeSlots.Push(slot);
        }
    }

    public UIInventorySlot GetSlot(Transform parent)
    {
        if(m_FreeSlots.Count > 0)
        {
            UIInventorySlot slot = m_FreeSlots.Pop();
            slot.transform.SetParent(parent);
            slot.gameObject.SetActive(true);
            return slot;
        }
        else
        {
            UIInventorySlot slot = Instantiate(m_SlotPrefab, parent, false);
            slot.gameObject.SetActive(true);
            return slot;
        }
    }

    public void ReturnSlot(UIInventorySlot slot)
    {
        if (slot == null)
        {
            return;
            
        }
        slot.Clear();
        slot.transform.SetParent(transform);
        slot.gameObject.SetActive(false);
        if (m_FreeSlots.Contains(slot))
        {
            return;
        }

        m_FreeSlots.Push(slot);

    }

    public void ReturnSlots(List<UIInventorySlot> slots)
    {
        foreach(var slot in slots)
        {
            ReturnSlot(slot);
        }
    }
}

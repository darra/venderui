﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class UIInventorySlot : UISelectableSlot
{
    public Action<InventoryUnit, EItemActionType> OnActionRequest;

    [SerializeField]
    private UISlotButton m_ButtonPrefab;
    [SerializeField]
    private TextMeshProUGUI m_ItemName;

    [SerializeField]
    private TextMeshProUGUI m_Count;

    [SerializeField]
    private Image m_Icon;

    private List<UISlotButton> m_Buttons = new List<UISlotButton>();

    private InventoryUnit m_Unit;

    public void Init(InventoryUnit unit, UIInventoryTooltip tooltip, float totalMoney)
    {
        m_Unit = unit;
        m_Unit.Slot = this;
        m_ItemName.text = unit.Data.Name;
        m_Icon.sprite = unit.Data.Asset;
        m_Tooltip = tooltip;
        m_TooltipData = new TooltipData(unit.Data, unit.GetMaterialName());
        SetCount();
        UpdateButtonsState(totalMoney);
        foreach (var action in unit.Actions)
        {
            UISlotButton button = Instantiate(m_ButtonPrefab, m_ButtonsRoot.transform, false);
            button.Init(action, unit.Data.Price);
            button.OnClick += HandleOnClick;
            m_Buttons.Add(button);
        }
        m_ButtonsRoot.gameObject.SetActive(false);
    }

    public void SetCount()
    {
        if (m_Unit.Count > 1)
        {
            m_Count.gameObject.SetActive(true);
            m_Count.text = m_Unit.Count.ToString();
        }
        else
        {
            m_Count.gameObject.SetActive(false);
        }
    }

    public void UpdateButtonsState(float totalMoney)
    {
        bool available = totalMoney >= m_Unit.Data.Price;
        m_TooltipData.Available = available;
        foreach (var button in m_Buttons)
        {
            button.UpdateState(available);
        }
    }

    private void HandleOnClick(EItemActionType action)
    {
        OnActionRequest.SafeInvoke(m_Unit, action);
    }

    public void Clear()
    {
        int buttonsCount = m_Buttons.Count;
        for (int i = buttonsCount - 1; i >= 0; i--)
        {
            m_Buttons[i].OnClick.RemoveListeners();
            Destroy(m_Buttons[i].gameObject);
            m_Buttons.Remove(m_Buttons[i]);
        }

        OnActionRequest = null;
        m_Unit.Slot = null;
        m_Unit = null;
    }
}

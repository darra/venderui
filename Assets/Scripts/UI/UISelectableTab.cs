﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;

public class UISelectableTab : UIBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    [SerializeField]
    protected EItemType m_ItemType;

    
    private UITabTooltip m_Tooltip;

    public void SetTooltipObject(UITabTooltip tooltip)
    {
        m_Tooltip = tooltip;
    }

    public void OnPointerEnter(PointerEventData data)
    {
        m_Tooltip.gameObject.SetActive(true);
        m_Tooltip.ShowTooltip(m_ItemType.ToString(), transform.position.x);
    }

    public void OnPointerExit(PointerEventData data)
    {
        m_Tooltip.gameObject.SetActive(false);
    }
}
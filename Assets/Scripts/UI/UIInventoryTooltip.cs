﻿using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInventoryTooltip : UIBehaviour
{
    [SerializeField] private TextMeshProUGUI m_Description;
    [SerializeField] private TextMeshProUGUI m_ItemName;
    [SerializeField] private TextMeshProUGUI m_AdditionalInfo;
    [SerializeField] private TextMeshProUGUI m_Prices;
    [SerializeField] private Image m_Image;

    public void ShowTooltip(TooltipData tooltipData, float yPosition)
    {
        //transform.position = new Vector3(transform.position.x, yPosition);
        m_Description.text = tooltipData.Data.Description;
        m_ItemName.text = tooltipData.Data.Name;
        string priceText = tooltipData.Available
            ? $"Price : {tooltipData.Data.Price}"
            : $"<color=\"red\">[Not enough money]</color> Price :<color=\"red\"> {tooltipData.Data.Price}";
        m_Prices.text = priceText;
        m_Image.sprite = tooltipData.Data.Asset;

        if (tooltipData.AdditionalInfo != null)
        {
            m_AdditionalInfo.gameObject.SetActive(true);
            StringBuilder sb = new StringBuilder();
            foreach (var info in tooltipData.AdditionalInfo)
            {
                sb.AppendLine(info);
            }
            m_AdditionalInfo.text = sb.ToString();
        }
        else
        {
            m_AdditionalInfo.gameObject.SetActive(false);
        }
    }
}

public class TooltipData
{
    public ItemData Data { get;}
    public bool Available { get; set; }
    public List<string> AdditionalInfo { get;}
    public TooltipData(ItemData data, List<string> additionalInfo)
    {
        Data = data;
        AdditionalInfo = additionalInfo;
    }

}

﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using TMPro;
using UnityEngine.EventSystems;

public class UIInventoryScrollView : UIBehaviour
{
    [SerializeField]
    private Transform m_SlotsRoot;
    [SerializeField]
    private SlotObjectPool m_SlotPool;
    [SerializeField]
    private UIInventoryTooltip m_Tooltip;
    [SerializeField]
    private UITabsGroup m_Tabs;
    [SerializeField]
    private TextMeshProUGUI m_PlayerMoney;
    [SerializeField]
    private UIAcceptancePopup m_AcceptancePopup;
    [SerializeField]
    private UISetAmountPopup m_SetAmountPopup;

    //private Dictionary<InventoryUnit, UIInventorySlot> m_CurrentSlots = new Dictionary<InventoryUnit, UIInventorySlot>();
    private List<InventoryUnit> m_CurrentSlots = new List<InventoryUnit>();
    private InventoryManager m_Manager;
    private float m_CurrentMoney;

    public void Init(InventoryManager manager, float opponentMoney)
    {
        m_Manager = manager;
        m_CurrentMoney = opponentMoney;
        m_Manager.OnRemoveItem += DeleteSlot;
        m_Manager.OnAddItem += AddSlot;
        m_Tabs.OnTabSelected += ChangeToogle;
        AddSlots(m_Manager.GetAllItems());
        HandleMoneyUpdate(m_Manager.TotalMoney, opponentMoney);
    }

    public void HandleMoneyUpdate(float totalMoney, float opponentMoney)
    {
        m_CurrentMoney = opponentMoney;
        m_PlayerMoney.text = totalMoney.ToString();
        foreach (var slot in m_CurrentSlots)
        {
            slot.Slot.UpdateButtonsState(opponentMoney);
        }
    }

    private void HandleActionRequest(InventoryUnit unit, EItemActionType action)
    {
        switch (action)
        {
            case EItemActionType.Use:
                m_Manager.HandleAction(unit, action, 1);
                break;
            case EItemActionType.Sell:
            case EItemActionType.Buy:
                int maxCount = m_Manager.GetTotalAmountOfItem(unit.Data);
                if (maxCount == 1)
                {
                    m_Manager.HandleAction(unit, action, maxCount);
                    break;
                }
                m_SetAmountPopup.gameObject.SetActive(true);
                m_SetAmountPopup.ShowPopup(maxCount);
                m_SetAmountPopup.OnResult = (int result) =>
                {
                    if (result > 0)
                    {
                        m_Manager.HandleAction(unit,action, result);
                    }
                };

                break;
            case EItemActionType.Disassemble:
                m_AcceptancePopup.gameObject.SetActive(true);
                m_AcceptancePopup.ShowPopup(unit.GetMaterialName());
                m_AcceptancePopup.OnResult = (bool result) =>
                {
                    if (result)
                    {
                        m_Manager.HandleAction(unit,EItemActionType.Disassemble, 1);
                    }
                };
                break;
        }
    }

    public void ChangeToogle(EItemType itemType)
    {
        if (m_Manager == null)
        {
            return;
        }

        foreach (var slot in m_CurrentSlots)
        {
            m_SlotPool.ReturnSlot(slot.Slot);
        }

        m_CurrentSlots.Clear();
        if (itemType == EItemType.None)
        {
            AddSlots(m_Manager.GetAllItems());
        }
        else
        {
            AddSlots(m_Manager.GetItemsByType(itemType));
        }
    }

    private void AddSlots(List<InventoryUnit> units)
    {
        if (units == null)
        {
            return;
        }
        foreach (var unit in units)
        {
            if (!m_CurrentSlots.Contains(unit))
            {
                AddSlot(unit);
            }
        }
    }

    private void AddSlot(InventoryUnit unit)
    {
        if (m_Tabs.CurrentSelectedTab != unit.Data.GetType() && m_Tabs.CurrentSelectedTab != EItemType.None)
        {
            return;
        }
        UIInventorySlot slot = m_SlotPool.GetSlot(m_SlotsRoot);
        slot.Init(unit, m_Tooltip, m_CurrentMoney);
        slot.OnActionRequest += HandleActionRequest;
        m_CurrentSlots.Add(unit);
    }

    private void DeleteSlot(InventoryUnit unit)
    {
        m_SlotPool.ReturnSlot(unit.Slot);
        m_CurrentSlots.Remove(unit);
    }

    public void SortByName()
    {
        m_CurrentSlots = m_CurrentSlots.OrderBy(u => u.Data.Name).ToList();

        for (int i = 0; i < m_CurrentSlots.Count; i++)
        {
            m_CurrentSlots[i].Slot.transform.SetSiblingIndex(i);
        }
    }

    public void SortByPrice()
    {
        m_CurrentSlots = m_CurrentSlots.OrderBy(u => u.Data.Price).ToList();

        for (int i = 0; i < m_CurrentSlots.Count; i++)
        {
            m_CurrentSlots[i].Slot.transform.SetSiblingIndex(i);
        }
    }
}

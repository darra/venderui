﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UISelectableSlot : UIBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    [SerializeField]
    protected Transform m_ButtonsRoot;

    protected UIInventoryTooltip m_Tooltip;
    protected TooltipData m_TooltipData;

    public void OnPointerEnter(PointerEventData data)
    {
        m_Tooltip.gameObject.SetActive(true);
        m_Tooltip.ShowTooltip(m_TooltipData, transform.position.y);
        m_ButtonsRoot.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData data)
    {
        m_Tooltip.gameObject.SetActive(false);
        m_ButtonsRoot.gameObject.SetActive(false);
    }
}

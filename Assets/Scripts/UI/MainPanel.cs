﻿using System;
using UnityEngine;

public class MainPanel: MonoBehaviour
{
    [SerializeField]
    private InventoryData m_PlayerInventoryData;
    [SerializeField]
    private InventoryData m_VendorInventoryData;

    [SerializeField]
    private UIInventoryScrollView m_PlayerInventory;

    [SerializeField]
    private UIInventoryScrollView m_VendorInventory;

    private InventoryManager m_PlayerManager;
    private InventoryManager m_VendorManager;


    private void Awake()
    {
        m_PlayerManager = new InventoryManager();
        m_PlayerManager.CreateInventory(m_PlayerInventoryData, HandleChangePlayerMoney);
        m_PlayerManager.OnTransaction = HandlePlayerTransaction;

        m_VendorManager = new InventoryManager();
        m_VendorManager.CreateInventory(m_VendorInventoryData, HandleChangeVendorMoney);
        m_VendorManager.OnTransaction = HandleVendorTransaction;

        m_PlayerInventory.Init(m_PlayerManager, m_VendorManager.TotalMoney);
        m_VendorInventory.Init(m_VendorManager, m_PlayerManager.TotalMoney);


    }

    private void HandleChangePlayerMoney(float currentAmount)
    {
        m_PlayerInventory.HandleMoneyUpdate(currentAmount, m_VendorManager.TotalMoney);
        m_VendorInventory.HandleMoneyUpdate (m_VendorManager.TotalMoney, currentAmount);
    }

    private void HandleChangeVendorMoney(float currentAmount)
    {
        m_PlayerInventory.HandleMoneyUpdate(m_PlayerManager.TotalMoney, currentAmount);
        m_VendorInventory.HandleMoneyUpdate(currentAmount, m_PlayerManager.TotalMoney);
    }

    private bool HandlePlayerTransaction(ItemData item, int count)
    {
        return SellerHelper.TryMakeTransaction(item, count, m_PlayerManager, m_VendorManager);
    }

    private bool HandleVendorTransaction(ItemData item, int count)
    {
        return SellerHelper.TryMakeTransaction(item, count, m_VendorManager, m_PlayerManager);

    }
}

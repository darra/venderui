﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class UILoadingScreen : UIBehaviour
{
    [SerializeField]
    private CanvasGroup m_CanvasGroup;

    public void OnButton()
    {
        StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        for (float f = 1f; f >= 0; f -= 0.05f)
        {
            m_CanvasGroup.alpha = f;
            yield return null;
        }

        m_CanvasGroup.alpha = 0;
        gameObject.SetActive(false);
    }
}

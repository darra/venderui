﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class UITabTooltip :UIBehaviour
{
    [SerializeField] private TextMeshProUGUI m_Title;

    public void ShowTooltip(string title, float xPosition)
    {
        m_Title.text = title;
        transform.position = new Vector3(xPosition, transform.position.y);
    }
}
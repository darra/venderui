﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISlotButton : UIBehaviour
{
    public Action<EItemActionType> OnClick;

    private const string Sell = "Sell";
    private const string Buy = "Buy";
    private const string Disassemble = "Disassemble";
    private const string Use = "Use";

    [SerializeField]
    private TextMeshProUGUI m_Title;

    [SerializeField]
    private TextMeshProUGUI m_Price;

    [SerializeField]
    private UIButton m_Button;

    private EItemActionType m_ActionType;
    private float m_PriceAmount;

    public void Init(EItemActionType actionType, float sellPrice)
    {
        m_ActionType = actionType;
        switch (actionType)
        {
          case EItemActionType.Buy:
                m_Title.text = Buy;
                m_PriceAmount = sellPrice;
                m_Price.gameObject.SetActive(true);
                m_Price.text = sellPrice.ToString();
                break;
            case EItemActionType.Sell:
                m_Title.text = Sell;
                m_PriceAmount = sellPrice;
                m_Price.gameObject.SetActive(true);
                m_Price.text = sellPrice.ToString();
                break;
            case EItemActionType.Disassemble:
                m_Title.text = Disassemble;
                m_Price.gameObject.SetActive(false);
                break;
            case EItemActionType.Use:
                m_Title.text = Use;
                m_Price.gameObject.SetActive(false);
                break;
        }
    }

    public void UpdateState(bool available)
    {
        if (m_ActionType == EItemActionType.Buy || m_ActionType == EItemActionType.Sell)
        {
            m_Button.interactable = available;
            m_Price.color = available ? Color.white : Color.red;
        }
    }

    public void OnClickHandle()
    {
        OnClick.SafeInvoke(m_ActionType);
    }

}
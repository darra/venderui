﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class UISetAmountPopup : UIBehaviour
{
    public Action<int> OnResult;

    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private Slider m_Slider;

    [SerializeField]
    private TextMeshProUGUI m_Amount;

    private int m_Max;

    public void  ShowPopup(int max)
    {
        m_Text.text = "Set amount of items";
        m_Slider.minValue = 1;
        m_Slider.maxValue = max;
        m_Slider.value = 1;
        m_Slider.onValueChanged.AddListener(OnSliderChangeAmount);
        m_Amount.text = "1";
    }

    private void OnSliderChangeAmount(float value)
    {
        m_Amount.text = value.ToString();
    }

    public void OnAccept()
    {
        int amount = Mathf.RoundToInt(m_Slider.value);
        OnResult.SafeInvoke(amount);
        gameObject.SetActive(false);
    }

    public void OnDecline()
    {
        OnResult.SafeInvoke(-1);
        gameObject.SetActive(false);
    }
}
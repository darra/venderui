﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class UITabsGroup : ToggleGroup
{
    [SerializeField]
    private List<UITabToggle> m_TabToogles;

    [SerializeField]
    private UITabTooltip m_Tooltip;


    public Action<EItemType> OnTabSelected;

    public EItemType CurrentSelectedTab { get; private set; }

    private void Awake()
    {
        base.Awake();
        CurrentSelectedTab = EItemType.None;
        foreach (var tab in m_TabToogles)
        {
            tab.OnTabSelect += OnToggelChange;
            tab.SetTooltipObject(m_Tooltip);
        }
    }

    public void OnToggelChange(EItemType type)
    {
        if (type != CurrentSelectedTab)
        {
            CurrentSelectedTab = type;
            OnTabSelected.SafeInvoke(CurrentSelectedTab);
        }
    }
}

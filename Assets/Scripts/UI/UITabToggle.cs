﻿using System;
using UnityEngine;
using Assets.Scripts;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class UITabToggle : UISelectableTab
{
    public Action<EItemType> OnTabSelect;

    private Toggle m_Toggle;

    private void Awake()
    {
        m_Toggle = GetComponent<Toggle>();
        m_Toggle.onValueChanged.AddListener(OnValueChange);
    }

    private void OnValueChange(bool enable)
    {
        if (enable)
        {
            OnTabSelect.SafeInvoke(m_ItemType);
        }
    }
}

﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine.EventSystems;

public class UIAcceptancePopup : UIBehaviour
{
    public Action<bool> OnResult;
    [SerializeField] private TextMeshProUGUI m_Text;

    public void ShowPopup(List<string> args)
    {
        gameObject.SetActive(true);
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("Disassembling action cannot be undone!");
        sb.AppendLine();
        if (args != null && args.Count >0)
        {
            foreach (var mat in args)
            {
                sb.AppendLine(mat);
            }

            sb.AppendLine();
        }

        sb.AppendLine("Proceed action?");
        m_Text.text = sb.ToString();
    }

    public void OnAccept()
    {
        OnResult.SafeInvoke(true);
        gameObject.SetActive(false);
    }

    public void OnDecline()
    {
        OnResult.SafeInvoke(false);
        gameObject.SetActive(false);
    }
}

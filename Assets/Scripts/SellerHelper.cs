﻿public static class SellerHelper
{
    public static bool TryMakeTransaction(ItemData itemToSell, int count, InventoryManager seller, InventoryManager buyer)
    {
        if (itemToSell.Price * count > buyer.TotalMoney)
        {
            return false;
        }

        buyer.TotalMoney -= count * itemToSell.Price;
        seller.TotalMoney += count * itemToSell.Price;
        buyer.AddItem(itemToSell, count);

        return true;
    }
}
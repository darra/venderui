﻿using System;
using System.Collections.Generic;

public class InventoryUnit
{
    public ItemData Data { get; private set; }
    public string Name { get; private set; }
    public int Count {
        get
        {
            return m_Count;
        }
        set
        {
            m_Count = value;
            if (Slot != null)
            {
                Slot.SetCount();
            }
        }
    }
    private int m_Count;
    public List<EItemActionType> Actions { get; private set; }

    public UIInventorySlot Slot;

    public void Init(ItemData data, int count, List<EItemActionType> possibleActions)
    {
        Data = data;
        Name = data.Name;
        m_Count = count;
        Actions = possibleActions;
    }

    public void Clear()
    {
        Slot = null;
    }

    public List<string> GetMaterialName()
    {
        if (Data as WeaponData)
        {
            WeaponData weaponData = (WeaponData) Data;
            if (weaponData != null)
            {
                List<string> names = new List<string>();
                names.Add("Materials after disassembling:");
                foreach (var material in weaponData.Compilation)
                {
                    names.Add(material.Name);
                }
                return names;
            }
        }
        return null;
    }
}
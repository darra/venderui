﻿using UnityEngine;
using UnityEditor;

public enum EItemActionType
{ 
    Sell = 0,
    Buy = 1,
    Use = 2,
    Disassemble = 3
}
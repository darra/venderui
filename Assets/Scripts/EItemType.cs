﻿namespace Assets.Scripts
{
    public enum EItemType
    { 
        Weapon = 1,
        Consumable = 2,
        Material = 3,
        QuestItem = 4,

        None = 10
    }
}

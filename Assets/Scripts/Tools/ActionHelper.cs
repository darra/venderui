﻿using System;

public static class ActionHelper
{
    public static void SafeInvoke<T>(this Action<T> action, T obj1)
    {
        if (action != null)
        {
            action.Invoke(obj1);
        }
    }

    public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 obj1, T2 obj2)
    {
        if (action != null)
        {
            action.Invoke(obj1, obj2);
        }
    }


    public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 obj1, T2 obj2, T3 obj3)
    {
        if (action != null)
        {
            action.Invoke(obj1, obj2, obj3);
        }
    }

    public static void SafeInvoke(this Action action)
    {
        if (action != null)
        {
            action.Invoke();
        }
    }

    public static void RemoveListeners<T>(this Action<T> action)
    {
        if (action == null)
        {
            return;
        }
        foreach (var _delegate in action.GetInvocationList())
        {
            action -= (Action<T>)_delegate;
        }

    }

    public static void RemoveListeners<T1, T2>(this Action<T1, T2> action)
    {
        if (action == null)
        {
            return;
        }
        foreach (var _delegate in action.GetInvocationList())
        {
            action -= (Action<T1, T2>)_delegate;
        }
    }

    public static void RemoveListeners<T1, T2, T3>(this Action<T1, T2, T3> action)
    {
        if (action == null)
        {
            return;
        }
        foreach (var _delegate in action.GetInvocationList())
        {
            action -= (Action<T1, T2, T3>)_delegate;
        }
    }


    public static void RemoveListeners(this Action action)
    {
        if (action == null)
        {
            return; 
        }
        foreach (var _delegate in action.GetInvocationList())
        {
            action -= (Action)_delegate;
        }
    }



}
﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DataSpawner : EditorWindow
{
    [MenuItem("Window/My Window")]

    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(DataSpawner));
    }

    private int count;
    private string name1;
    private string name2;
    private string name3;

    private string description1;
    private string description2;
    private string description3;

    private Sprite sprite1;
    private Sprite sprite2;
    private Sprite sprite3;

    private MaterialData mat1;
    private MaterialData mat2;
    private MaterialData mat3;

    private float minPrice;
    private float maxPrice;

    void OnGUI()
    {

        count = EditorGUILayout.IntField(count);

        minPrice = EditorGUILayout.FloatField("min price",minPrice);
        maxPrice = EditorGUILayout.FloatField("max price",maxPrice);

        name1 = EditorGUILayout.TextField("Possible name1", name1);
        name2 = EditorGUILayout.TextField("Possible name2", name2);
        name3 = EditorGUILayout.TextField("Possible name3", name3);

        List<string> names = new List<string>()
        {
            name1, name2, name3
        };

        description1 = EditorGUILayout.TextField("Possible description1", description1);
        description2 = EditorGUILayout.TextField("Possible description2", description2);
        description3 = EditorGUILayout.TextField("Possible description3", description3);
        List<string> descriptions = new List<string>()
        {
            description1, description2, description3
        };

        sprite1 = (Sprite)EditorGUILayout.ObjectField(sprite1, typeof(Sprite));
        sprite2 = (Sprite)EditorGUILayout.ObjectField(sprite2, typeof(Sprite));
        sprite3 = (Sprite)EditorGUILayout.ObjectField(sprite3, typeof(Sprite));
        List<Sprite> sprites = new List<Sprite>()
        {
            sprite1, sprite2, sprite3
        };

        mat1 = (MaterialData)EditorGUILayout.ObjectField(mat1, typeof(MaterialData));
        mat2 = (MaterialData)EditorGUILayout.ObjectField(mat2, typeof(MaterialData));
        mat3 = (MaterialData)EditorGUILayout.ObjectField(mat3, typeof(MaterialData));
        List<MaterialData> mats = new List<MaterialData>()
        {
            mat1, mat2, mat3
        };


        if (GUILayout.Button("Generate Weapon Object"))
        {
            for (int i = 0; i < count; i++)
            {
                string name = names[UnityEngine.Random.Range(0, 3)];
                string desc = descriptions[UnityEngine.Random.Range(0, 3)];
                Sprite sprite = sprites[UnityEngine.Random.Range(0, 3)];
                int sellPrice = Mathf.RoundToInt(UnityEngine.Random.Range(minPrice, maxPrice));
                int randomInt = UnityEngine.Random.Range(0, 1000);
                WeaponData data = ScriptableObject.CreateInstance<WeaponData>();
                AssetDatabase.CreateAsset(data, "Assets/Data/Weapons/Weapon_" + name + "_" + randomInt + ".asset");
                AssetDatabase.SaveAssets();

                data.InitItem(name, desc, sprite, sellPrice);

                data.Compilation = new List<MaterialData>() {
                    mats[UnityEngine.Random.Range(0, 3)],
                    mats[UnityEngine.Random.Range(0, 3)]};
                EditorUtility.SetDirty(data);
            }
        }

        if (GUILayout.Button("Generate Quest Object"))
        {
            for (int i = 0; i < count; i++)
            {
                string name = names[UnityEngine.Random.Range(0, 3)];
                string desc = descriptions[UnityEngine.Random.Range(0, 3)];
                Sprite sprite = sprites[UnityEngine.Random.Range(0, 3)];
                int sellPrice = Mathf.RoundToInt(UnityEngine.Random.Range(minPrice, maxPrice));
                int randomInt = UnityEngine.Random.Range(0, 1000);
                QuestItemData data = ScriptableObject.CreateInstance<QuestItemData>();
                AssetDatabase.CreateAsset(data, "Assets/Data/Quest/Quest_" + name + "_" + randomInt + ".asset");
                AssetDatabase.SaveAssets();

                data.InitItem(name, desc, sprite, sellPrice);
                EditorUtility.SetDirty(data);
            }
        }

        if (GUILayout.Button("Generate Material Object"))
        {
            for (int i = 0; i < count; i++)
            {
                string name = names[UnityEngine.Random.Range(0, 3)];
                string desc = descriptions[UnityEngine.Random.Range(0, 3)];
                Sprite sprite = sprites[UnityEngine.Random.Range(0, 3)];
                int sellPrice = Mathf.RoundToInt(UnityEngine.Random.Range(minPrice, maxPrice));
                int randomInt = UnityEngine.Random.Range(0, 1000);
                MaterialData data = ScriptableObject.CreateInstance<MaterialData>();
                AssetDatabase.CreateAsset(data, "Assets/Data/Materials/Material_" + name + "_" + randomInt + ".asset");
                AssetDatabase.SaveAssets();

                data.InitItem(name, desc, sprite, sellPrice);
                EditorUtility.SetDirty(data);
            }
        }

        if (GUILayout.Button("Generate Consumable Object"))
        {
            for (int i = 0; i < count; i++)
            {
                string name = names[UnityEngine.Random.Range(0, 3)];
                string desc = descriptions[UnityEngine.Random.Range(0, 3)];
                Sprite sprite = sprites[UnityEngine.Random.Range(0, 3)];
                int sellPrice = Mathf.RoundToInt(UnityEngine.Random.Range(minPrice, maxPrice));
                int randomInt = UnityEngine.Random.Range(0, 1000);
                ConsumableData data = ScriptableObject.CreateInstance< ConsumableData>();
                AssetDatabase.CreateAsset(data, "Assets/Data/Consumable/Consumable_" + name + "_" + randomInt + ".asset");
                AssetDatabase.SaveAssets();

                data.InitItem(name, desc, sprite, sellPrice);
                EditorUtility.SetDirty(data);

            }
        }
    }
}
#endif

